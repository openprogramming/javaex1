package pl.op;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

public class TemplateReader {

	private String template = "";
	private String name;
	
	public TemplateReader(String f1, String f2) throws Exception
	{
		name = f2;
	    BufferedReader file = new BufferedReader(new FileReader(f1));
	    StringBuilder b = new StringBuilder();
	    String l;
	    while((l = file.readLine() ) != null )
	    {
	    	b.append(l);
	    	b.append("\n");
	    }
	    template = b.toString();
	    file.close();
	}
	
	public void addMembers(String s)
	{
		int p = template.indexOf("/*members*/");
		   
	    StringBuilder newStr = new StringBuilder();
	    newStr.append(template.substring(0, p) + "\n");
	    newStr.append(s + "\n");
	    newStr.append("\t" + template.substring(p, template.length()));

	    template = newStr.toString();
	}
	
	
	public void AddGetSet(String gs)
	{
		int p = template.indexOf("/*get-set*/");
	   
	    StringBuilder newStr = new StringBuilder();
	    newStr.append(template.substring(0, p) + "\n");
	    newStr.append(gs + "\n");
	    newStr.append("\t" + template.substring(p, template.length()));

	    template = newStr.toString();
	}
	
	public void Generate() throws Exception
	{
		PrintWriter  fs = new PrintWriter(name);
		fs.print(template);
		fs.close();
		System.out.println(template);
	}
}
