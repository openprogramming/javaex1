package pl.op;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Parser {
	
	private ArrayList<String> names = new ArrayList<String>();
	private ArrayList<String> types = new ArrayList<String>();
	
	public Parser( String name ) throws Exception
	{
	    TemplateReader templates = new TemplateReader("../MessageTester/res/Template.txt",
	    		"../MessageTester/src/pl/op/tester/Concrete.java");
	    
	    BufferedReader file = new BufferedReader(new FileReader(name));
	    StringBuilder b = new StringBuilder();
	    String l;
	    while((l = file.readLine() ) != null )
	    {
	    	b.append(l);
	    }
	    file.close();

	    String rep = b.toString();
	    String[] sl = rep.split("\\|");
	    for(int i = 0; i < sl.length; ++i)
	    {
	    	String[] member = sl[i].split(":");
	    	names.add(member[0]);
	    	types.add(member[1]);
	    	
	    	String s1 = member[0];
	    	String s2 = member[1];
	    	s2 = s2.toUpperCase();
	    	
	    	StringBuilder addition1 = new StringBuilder();
	    	StringBuilder addition2 = new StringBuilder();
	    	StringBuilder addition3 = new StringBuilder();
	    	StringBuilder addition4 = new StringBuilder();

	    	if(s2.equals("INT"))
	    	{
	    		addition1.append("\tint m_" + s1 + ";");
	    		templates.addMembers(addition1.toString());
	    		
	    		
	    		addition2.append("\tvoid set" + s1 + "( int " + s1 + " ){");
	    		addition2.append( " m_" + s1 + " = " + s1 + ";");
	    		addition2.append("}\n");
	    		addition2.append("\tint get" + s1 + "() {");
	    		addition2.append("return m_" + s1 + ";}\n");
	    		templates.AddGetSet(addition2.toString());
	    		
	    	}
	    	else if(s2.equals("BOOL"))
	    	{
	    		addition1.append("\tboolean m_" + s1 + ";");
	    		templates.addMembers(addition1.toString());
	    		
	    		
	    		addition2.append("\tvoid set" + s1 + "( boolean " + s1 + " ){");
	    		addition2.append( " m_" + s1 + " = " + s1 + ";");
	    		addition2.append("}\n");
	    		addition2.append("\tboolean get" + s1 + "() {");
	    		addition2.append("return m_" + s1 + ";}\n");
	    		templates.AddGetSet(addition2.toString());
	    		
	    	}
	    }

	    templates.Generate();
	}

	public void Print()
	{
		for( int i = 0; i < types.size(); ++i)
		{
			System.out.print(types.get(i));
		}
	}
}
