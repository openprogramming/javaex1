package pl.op.tester;

import java.io.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Concrete implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	int m_name1;
	
	boolean m_name2;
	/*members*/		

	
	void setname1( int name1 ){ m_name1 = name1;}
	int getname1() {return m_name1;}

	
	void setname2( boolean name2 ){ m_name2 = name2;}
	boolean getname2() {return m_name2;}

	/*get-set*/		

	public byte[] Write() throws IOException
	{
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		ObjectOutputStream stream = new ObjectOutputStream(byteStream);
		stream.writeObject(this);
		stream.close();
		byteStream.close();
		return byteStream.toByteArray();
	}
	
	public static Concrete Read(byte[] str) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream byteStream = new ByteArrayInputStream(str);
		ObjectInputStream stream = new ObjectInputStream(byteStream);
		
		Concrete obj = (Concrete) stream.readObject();
		stream.close();
		byteStream.close();
		
		return obj;
	}
}
